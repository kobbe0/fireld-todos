<?php
/**
 * @var $this Admin
 * @var $milestone_id int
 * @var $milestones array
 * @var $todos array
 * @var $status_counts array
 * @var $tags array
 */

?>
<div class="row">

	<div class="col-sm-4 col-lg-3">
		<div class="block full">
			<div class="block-title clearfix">
				<h2><i class="fa fa-tasks"></i> <b>Att göra</b> listor</h2>
			</div>
			<ul class="nav nav-pills nav-stacked">
				<?php foreach ($milestones as $milestone) { ?>
					<li class="<?=$milestone->id == $milestone_id ? 'active' : ''?>">
						<a href="/admin/todos/<?=$milestone->id?>">
							<span class="badge pull-right" data-toggle="tooltip" title="Ska göras eller verifieras"><?=$milestone->count?></span>
							<strong data-toggle="tooltip" title="<?=$milestone->description?>"><?=$milestone->name?></strong>
						</a>
					</li>
				<?php } ?>
			</ul>
		</div>
	</div>

	<div class="col-sm-8 col-lg-9">
		<div class="block">
			<?php if ( ! $milestone_id) { ?>
				<p>Välj en <b>Att göra</b> lista</p>
			<?php } else { ?>
				<div class="block-title">
					<div class="block-options pull-right">
						<a href="/<?=$this->uri->uri_string().'/add'?>" class="btn btn-alt btn-sm btn-default">
							<i class="fa fa-plus"></i>
						</a>
					</div>
					<h2><?=$milestones[$milestone_id]->name?></h2>
				</div>
				<p><?=$milestones[$milestone_id]->description?></p>
				<div class="block-content-full">
					<table class="table table-borderless table-striped table-vcenter">
						<thead>
						<tr>
							<td colspan="3">
								<ul class="list-inline remove-margin">
									<li>
										<i class="fa fa-question"></i>
										<span class="text-danger"><strong><?=$status_counts[10]?> Verifieras</strong></span>
									</li>
									<li>
										<i class="fa fa-warning"></i>
										<span class="text-warning"><strong><?=$status_counts[20]?> Ska göras</strong></span>
									</li>
									<li>
										<i class="fa fa-check"></i>
										<span class="text-success"><strong><?=$status_counts[30]?> Klara</strong></span>
									</li>
								</ul>
							</td>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($todos as $todo) { ?>
							<tr>
								<td class="text-center" style="width: 60px;">
									<?php if ($todo->status == 20) { ?>
										<a href="/admin/todos/manage/<?=$todo->id?>" class="text-warning">
											<i class="fa fa-warning fa-2x" data-toggle="tooltip" title="Ska göras"></i>
										</a>
									<?php } elseif ($todo->status == 10) { ?>
										<a href="/admin/todos/manage/<?=$todo->id?>" class="text-danger">
											<i class="fa fa-question fa-2x" data-toggle="tooltip" title="Verifieras"></i>
										</a>
									<?php } elseif ($todo->status == 30) { ?>
										<a href="/admin/todos/manage/<?=$todo->id?>" class="text-success">
											<i class="fa fa-check fa-2x" data-toggle="tooltip" title="Klart"></i>
										</a>
									<?php } ?>
								</td>
								<td>
									<strong><?=$todo->title?></strong>
									<span class="label label-primary" data-toggle="tooltip" title="<?=$todo->type_description?>"><?=strtolower($todo->type_name)?></span>
									<?php if ($todo->help) { ?>
										<span class="label label-danger" data-toggle="tooltip"
										      title="För att kunna slutföra denna krävs tydlig specifikation. Alternativt ska den inte göras av kodare.">mer info behövs</span>
									<?php } ?>
									<?php
									foreach ($todo->tag_ids as $tag_id) {
										if ( ! $tag_id) continue;
										?>
										<span class="label label-info" data-toggle="tooltip" title="<?=$tags[$tag_id]->description?>"><?=strtolower($tags[$tag_id]->name)?></span>
									<?php } ?>
									<?php if ($todo->status == 30) { ?>
										<span class="label label-success">klart <?=format_date($todo->done_at)?></span>
									<?php } ?>
									<div class="text-muted"><?=$todo->description?></div>
									<?php if ($todo->status == 30 && $todo->done_text) { ?>
										<div class="text-success"><?=$todo->done_text?></div>
									<?php } ?>
									<?php if ($todo->status != 30 && $todo->hold_text) { ?>
										<div class="text-danger"><?=$todo->hold_text?></div>
									<?php } ?>
								</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			<?php } ?>
		</div>
	</div>
</div>