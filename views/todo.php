<?php
/**
 * @var $this Admin
 * @var $id int
 * @var $milestone_id int
 * @var $todo object
 * @var $milestones array
 * @var $types array
 * @var $tags array
 */


if ( ! $id) {
	$todo = new stdClass();
	$todo->title = '';
	$todo->type_id = 0;
	$todo->milestone_id = $milestone_id;
}
?>

<form action="/<?=$this->uri->uri_string()?>" method="post" class="form-horizontal">
	<?php if ($id) { ?>
		<input type="hidden" name="id" value="<?=$id?>">
	<?php } ?>

	<div class="row">
		<div class="col-xs-12">
			<div class="block">
				<div class="block-section form-bordered">

					<div class="form-group">
						<label>Title</label>
						<input type="text" class="form-control" name="title" value="<?=str_replace('"', '&quot;', $todo->title)?>">
					</div>

					<div class="form-group">
						<label class="controll-label">Milestone</label>
						<select name="milestone_id" class="form-control">
							<option value="">Please select</option>
							<?php foreach ($milestones as $milestone) { ?>
								<option value="<?=$milestone->id?>"<?=$milestone->id === $todo->milestone_id ? ' selected' : ''?>><?=$milestone->name?></option>
							<?php } ?>
						</select>
					</div>

					<div class="form-group">
						<label class="controll-label">Type</label>
						<select name="type_id" class="form-control">
							<option value="">Please select</option>
							<?php foreach ($types as $type) { ?>
								<option value="<?=$type->id?>"<?=$type->id === $todo->type_id ? ' selected' : ''?>><?=$type->name?></option>
							<?php } ?>
						</select>
					</div>

					<?php if ($id) { ?>

						<div class="form-group">
							<label>Description</label>
							<input type="text" class="form-control" name="description" value="<?=$todo->description?>">
						</div>

						<div class="form-group">
							<label class="checkbox-inline">
								<input type="checkbox" name="help"<?=$todo->help ? ' checked' : ''?>>
								Mer info behövs
							</label>
						</div>

						<div class="form-group">
							<label class="controll-label">Status</label>
							<select name="status" class="form-control">
								<option value="">Please select</option>
								<option value="10"<?=10 == $todo->status ? ' selected' : ''?>>Verifiera</option>
								<option value="20"<?=20 == $todo->status ? ' selected' : ''?>>Att göra</option>
								<option value="30"<?=30 == $todo->status ? ' selected' : ''?>>Klart</option>
							</select>
						</div>

						<div class="form-group">
							<label>Tags</label><br>
							<?php foreach ($tags as $tag) { ?>
							<label class="checkbox-inline">
								<input type="checkbox" name="tags[<?=$tag->id?>]"<?=in_array($tag->id, $todo->tag_ids) ? ' checked' : ''?>>
								<?=$tag->name?>
							</label>
							<?php } ?>
						</div>

						<div class="form-group">
							<label class="text-danger">Hold info</label>
							<input type="text" class="form-control" name="hold_text" value="<?=$todo->hold_text?>">
						</div>

						<div class="form-group">
							<label class="text-success">Done at</label>
							<input type="text" class="form-control" name="done_at" value="<?=$todo->done_at ? format_date($todo->done_at, false) : ''?>">
						</div>

						<div class="form-group">
							<label class="text-success">Done info</label>
							<input type="text" class="form-control" name="done_text" value="<?=$todo->done_text?>">
						</div>

					<?php } ?>

					<div class="form-group form-actions">
						<button type="submit" class="btn btn-sm btn-primary">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</div>

</form>