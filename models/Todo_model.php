<?php
class Todo_model extends CI_Model
{
	public function get($id = null, $milestone_id = null)
	{
		$this->db->from('todos t');
		$this->db->join('todo_milestones tm', 'tm.id = t.milestone_id');
		$this->db->join('todo_types tt', 'tt.id = t.type_id');
		if ($id) {
			$this->db->where('t.id', $id);
		} elseif ($milestone_id) {
			$this->db->where('t.milestone_id', $milestone_id);
			$this->db->order_by('t.status');
			$this->db->order_by('t.done_at','desc');
			$this->db->order_by('t.updated_at');
		}
		$this->db->select('t.*');
		$this->db->select('tm.name milestone_name');
		$this->db->select('tt.name type_name, tt.description type_description');
		$result = $this->db->get();
		if ($id) {
			$return = $result->row();
			$return->tag_ids = explode(',', $return->tag_ids);
		} else {
			$return = [];
			foreach ($result->result() as $row) {
				$row->tag_ids = explode(',', $row->tag_ids);
				$return[] = $row;
			}
		}
		return $return;
	}

	public function post($id)
	{
		if ($id && $id != post('id')) return 'ID error!';

		$set = new stdClass();
		$set->title = post('title');
		$set->description = post('description');
		$set->done_text = post('done_text');
		$set->hold_text = post('hold_text');

		$set->milestone_id = post('milestone_id', 'numeric');
		$set->type_id = post('type_id', 'numeric');
		$set->status = post('status', 'numeric') ?: 20;

		$set->help = post('help') ? 1 : 0;

		$post_done_at = post('done_at');
		if ($post_done_at && false !== $done_at = strtotime($post_done_at) ) {
			$set->done_at = date('c', $done_at);
		}

		if ($set->status == 30 && empty($set->done_at)) {
			$set->done_at = date('c');
		}

		$tags = [];
		$tags_array = $this->input->post('tags');
		if (is_array($tags_array)) {
			foreach ($tags_array as $key => $value) {
				$tags[] = $key;
			}
		}
		$set->tag_ids = implode(',', $tags);

		if ( ! $set->title) return 'Title required';
		if ( ! $set->type_id) return 'Type required';

		$set->updated_at = date('c');

		if ($id) {
			$this->db->update('todos', $set, ['id' => $id]);
		} else {
			$set->created_at = date('c');
			$this->db->insert('todos', $set);
		}

		return true;
	}

	public function milestones_get()
	{
		$milestones = [];

		$this->db->from('todo_milestones');
		$this->db->order_by('sort, name');
		$result = $this->db->get();
		foreach ($result->result() as $row) {
			$row->count = 0;
			$milestones[$row->id] = $row;
		}

		$this->db->from('todos');
		$this->db->where('status !=', 30);
		$this->db->select('milestone_id');
		$result = $this->db->get();
		foreach ($result->result() as $row) {
			$milestones[$row->milestone_id]->count++;
		}

		return $milestones;
	}

	public function types_get()
	{
		$this->db->from('todo_types');
		$this->db->order_by('name');
		$result = $this->db->get();
		$types = [];
		foreach ($result->result() as $row) {
			$types[$row->id] = $row;
		}
		return $types;
	}

	public function tags_get()
	{
		$tags = [];

		$this->db->from('todo_tags');
		$result = $this->db->get();
		foreach ($result->result() as $row) {
			$tags[$row->id] = $row;
		}

		return $tags;
	}

}